//
//  AppDelegate.h
//  EyeControl
//
//  Created by Shashank Garg on 21/10/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

