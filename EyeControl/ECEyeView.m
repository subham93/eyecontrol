//
//  ECEyeView.m
//  EyeControl
//
//  Created by Shashank Garg on 21/10/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import "ECEyeView.h"

#define DRAGGABLE_VIEW_WIDTH  10

@implementation ECEyeView
{
    CGPoint p1, p2, p3, p4;
    UIView *leftDraggableView;
    UIView *rightDraggableView;
    UIView *topDraggableView;
    UIView *bottomDraggableView;
    
    UILongPressGestureRecognizer *leftPanResizeGesture;
    UILongPressGestureRecognizer *rightPanResizeGesture;
    UILongPressGestureRecognizer *topPanResizeGesture;
    UILongPressGestureRecognizer *bottomPanResizeGesture;
    
    UIBezierPath *bezierPath;
    
    UIImageView *imgView;
}

-(instancetype)initWithFrame:(CGRect)frame withPoint:(CGPoint)point {

    if (self = [super initWithFrame:frame]) {
        
        self.alpha = 0.5;
        
        p1 = CGPointMake(point.x-20, point.y);
        p2 = CGPointMake(point.x+20, point.y);
        p3 = CGPointMake(point.x, point.y-20);;
        p4 = CGPointMake(point.x, point.y+20);
        
        leftPanResizeGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(resizeTranslate:)];
        rightPanResizeGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(resizeTranslate:)];
        topPanResizeGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(resizeTranslate:)];
        bottomPanResizeGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(resizeTranslate:)];
        
        leftDraggableView = [self createDraggableViewWithFrame:CGRectMake(p1.x-DRAGGABLE_VIEW_WIDTH/2, p1.y-DRAGGABLE_VIEW_WIDTH/2, DRAGGABLE_VIEW_WIDTH, DRAGGABLE_VIEW_WIDTH) withGesture:leftPanResizeGesture];
        rightDraggableView = [self createDraggableViewWithFrame:CGRectMake(p2.x-DRAGGABLE_VIEW_WIDTH/2, p2.y-DRAGGABLE_VIEW_WIDTH/2, DRAGGABLE_VIEW_WIDTH, DRAGGABLE_VIEW_WIDTH) withGesture:rightPanResizeGesture];
        topDraggableView = [self createDraggableViewWithFrame:CGRectMake(p3.x-DRAGGABLE_VIEW_WIDTH/2, p3.y-DRAGGABLE_VIEW_WIDTH/2, DRAGGABLE_VIEW_WIDTH, DRAGGABLE_VIEW_WIDTH) withGesture:topPanResizeGesture];
        bottomDraggableView = [self createDraggableViewWithFrame:CGRectMake(p4.x-DRAGGABLE_VIEW_WIDTH/2, p4.y-DRAGGABLE_VIEW_WIDTH/2, DRAGGABLE_VIEW_WIDTH, DRAGGABLE_VIEW_WIDTH) withGesture:bottomPanResizeGesture];
        
        [self drawShapeLayer];
    }
    
    return self;
}

-(void)updateFramesWithPosition:(CGPoint)point {

    p1 = [self moveEachPoint:p1 withDistance:point];
    p2 = [self moveEachPoint:p2 withDistance:point];
    p3 = [self moveEachPoint:p3 withDistance:point];
    p4 = [self moveEachPoint:p4 withDistance:point];
    [self setFrameForImageView:leftDraggableView withPoint:p1];
    [self setFrameForImageView:rightDraggableView withPoint:p2];
    [self setFrameForImageView:topDraggableView withPoint:p3];
    [self setFrameForImageView:bottomDraggableView withPoint:p4];
    [self drawShapeLayer];
}

- (CGPoint)moveEachPoint:(CGPoint)point withDistance:(CGPoint)distanceMoved {

    point.x += distanceMoved.x;
    point.y += distanceMoved.y;
    return point;
}

- (UIView *)createDraggableViewWithFrame:(CGRect)frame withGesture:(UILongPressGestureRecognizer *)gesture {

    gesture.delegate = self;
//    gesture.cancelsTouchesInView = NO;
    UIView *vw = [[UIView alloc] initWithFrame:frame];
    [vw setUserInteractionEnabled:YES];
    [vw.layer setCornerRadius:frame.size.width/2];
    [vw setBackgroundColor:[UIColor blueColor]];
    [vw addGestureRecognizer:gesture];
    [self addSubview:vw];
    gesture.minimumPressDuration = 0.05;
    vw.exclusiveTouch = YES;
    
    return vw;
}

- (void)setFrameForImageView:(UIView *)view withPoint:(CGPoint)point {
    
    [view setFrame:CGRectMake(point.x-DRAGGABLE_VIEW_WIDTH/2, point.y-DRAGGABLE_VIEW_WIDTH/2, DRAGGABLE_VIEW_WIDTH, DRAGGABLE_VIEW_WIDTH)];
}

-(BOOL)shouldMoveViewForTouchInPoint:(CGPoint)point {
    
    if (_isControlViewPanning && !_areControlsPanning)
        return YES;
    else if (CGPathContainsPoint(_shapeView.path, NULL, point, NO) && !_areControlsPanning)
        return YES;
    else
        return NO;
    
//    if (CGRectContainsPoint(leftDraggableView.frame, point) ||
//        CGRectContainsPoint(rightDraggableView.frame, point) ||
//        CGRectContainsPoint(topDraggableView.frame, point) ||
//        CGRectContainsPoint(bottomDraggableView.frame, point)) {
        
//        return NO;
//    } else
//        return YES;
}

-(void)resizeTranslate:(UILongPressGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateBegan)
        _areControlsPanning = YES;
    
    CGPoint point = [recognizer locationInView:self];
    if (recognizer == leftPanResizeGesture) {
        p1 = point;
        [self setFrameForImageView:leftDraggableView withPoint:p1];
    }
    else if (recognizer == rightPanResizeGesture) {
        p2 = point;
        [self setFrameForImageView:rightDraggableView withPoint:p2];
    }
    else if (recognizer == topPanResizeGesture) {
        p3 = point;
        [self setFrameForImageView:topDraggableView withPoint:p3];
    }
    else if (recognizer == bottomPanResizeGesture) {
        p4 = point;
        [self setFrameForImageView:bottomDraggableView withPoint:p4];
    }
    [self drawShapeLayer];
    
    if (recognizer.state == UIGestureRecognizerStateEnded)
        _areControlsPanning= NO;
}

//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//    
//    CGPoint point = [[touches anyObject] locationInView:self];
//    if (CGRectContainsPoint(leftImgView.frame, point)) {
//        p1 = point;
//        [self setFrameForImageView:leftImgView withPoint:p1];
//    }
//    else if (CGRectContainsPoint(rightImgView.frame, point)) {
//        p2 = point;
//        [self setFrameForImageView:rightImgView withPoint:p2];
//    }
//    else if (CGRectContainsPoint(topImgView.frame, point)) {
//        p3 = point;
//        [self setFrameForImageView:topImgView withPoint:p3];
//    }
//    else if (CGRectContainsPoint(bottomImgView.frame, point)) {
//        p4 = point;
//        [self setFrameForImageView:bottomImgView withPoint:p4];
//    }
//    [self setNeedsDisplay];
//}

-(void)drawBeizer {
    
    [bezierPath moveToPoint:CGPointMake(p1.x, p1.y)];
    [bezierPath addQuadCurveToPoint:CGPointMake(p2.x, p2.y) controlPoint:p4];
    [bezierPath addQuadCurveToPoint:CGPointMake(p1.x, p1.y) controlPoint:p3];
}

- (void)drawShapeLayer {
    
    bezierPath = nil;
    [_shapeView removeFromSuperlayer];
    _shapeView = nil;
    bezierPath = [UIBezierPath bezierPath];
    _shapeView = [[CAShapeLayer alloc] init];
    [_shapeView setFillColor:[UIColor brownColor].CGColor];

    [self setNeedsDisplay];
    [self drawBeizer];
    [_shapeView setPath:bezierPath.CGPath];
    [[self layer] addSublayer:_shapeView];
}

- (BOOL)gestureRecognizer:(UILongPressGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UISwipeGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end
