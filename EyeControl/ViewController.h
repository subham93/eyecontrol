//
//  ViewController.h
//  EyeControl
//
//  Created by Shashank Garg on 21/10/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate>

@property (strong, nonatomic) UIImage *image;

@end

