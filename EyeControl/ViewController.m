//
//  ViewController.m
//  EyeControl
//
//  Created by Shashank Garg on 21/10/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import "ViewController.h"
#import "ECEyeView.h"

#define SLIDER_MULTIPLIER 40

typedef enum {
    INITIAL_VIEW,
    LEFT_EYE_MARKERS,
    LEFT_EYE_CIRCLE,
    RIGHT_EYE_MARKERS,
    RIGHT_EYE_CIRCLE,
    FINAL_VIEW
}CONTROL_TYPE;

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *leftCircleContainerView;
@property (weak, nonatomic) IBOutlet UIView *rightCircleContainerView;
@property (assign, nonatomic) CGPoint leftEyePos;
@property (assign, nonatomic) CGPoint rightEyePos;
@property (assign, nonatomic) CONTROL_TYPE shownControlType;

@end

@implementation ViewController
{
    ECEyeView *leftEyeVw;
    ECEyeView *rightEyeVw;
    
    UIView *leftEyeCircle;
    UIView *rightEyeCircle;
    
    UIImageView *leftCircleImgView;
    UIImageView *rightCircleImgView;
    
    UISlider *slider;
    
    CGPoint leftEyeControlPrevPoint;
    CGPoint rightEyeControlPrevPoint;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.minimumZoomScale = 1;
    self.scrollView.maximumZoomScale = 20.0;
    self.scrollView.clipsToBounds = YES;
    
    self.scrollView.zoomScale = 1.0; // reset zoomScale for new image
//    if (!_image)
//        _image = [UIImage imageNamed:@"test1.jpg"];
//    self.scrollView.contentSize = CGSizeMake(_image.size.width/2, _image.size.height/2);
    self.scrollView.scrollEnabled = NO;
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
//    NSString *imgPath = [[NSBundle mainBundle] pathForResource:@"test1" ofType:@"jpg"];
//    CIImage *img;
//    if (_image)
//        img = [[CIImage alloc] initWithCGImage:_image.CGImage];
//    else
//        img = [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:imgPath]];
    _shownControlType = LEFT_EYE_MARKERS;
//    [self fetchEyePositionWithImage:img];
    
    UIBarButtonItem *newPicItem = [[UIBarButtonItem alloc] initWithTitle:@"+New" style:UIBarButtonItemStylePlain target:self action:@selector(pickPhoto:)];
    self.navigationItem.leftBarButtonItem = newPicItem;
}

- (IBAction)pickPhoto:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
//    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    _imgView.image = chosenImage;
    self.scrollView.contentSize = CGSizeMake(chosenImage.size.width/2, chosenImage.size.height/2);
    self.scrollView.zoomScale = 1;
    [self resetView];
    _shownControlType = LEFT_EYE_MARKERS;
    [self fetchEyePositionWithImage:[[CIImage alloc] initWithCGImage:chosenImage.CGImage]];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)gotoNextControl:(id)sender {

    _shownControlType += 1;
    
    [self hideAllControls];
    [self handleControls];
}

- (void)handleControls {
    
    if (_shownControlType == LEFT_EYE_MARKERS) {
        [self.scrollView zoomToRect:CGRectMake(_leftEyePos.x-50, _leftEyePos.y-50, 100, 100) animated:YES];
        leftEyeVw.hidden = NO;
        [_containerView bringSubviewToFront:_imgView];
        UIBarButtonItem *nextItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(gotoNextControl:)];
        self.navigationItem.rightBarButtonItem = nextItem;
    } else if (_shownControlType == LEFT_EYE_CIRCLE) {
        slider.hidden = NO;
        leftEyeCircle.hidden = NO;
        slider.value = CGRectGetHeight(leftEyeCircle.frame)/SLIDER_MULTIPLIER;
        [_containerView bringSubviewToFront:_leftCircleContainerView];
        [_leftCircleContainerView bringSubviewToFront:leftCircleImgView];
    } else if (_shownControlType == RIGHT_EYE_MARKERS) {
        [self.scrollView zoomToRect:CGRectMake(_rightEyePos.x-50, _rightEyePos.y-50, 100, 100) animated:YES];
        rightEyeVw.hidden = NO;
        [_containerView bringSubviewToFront:_imgView];
    } else if (_shownControlType == RIGHT_EYE_CIRCLE) {
        slider.hidden = NO;
        rightEyeCircle.hidden = NO;
        slider.value = CGRectGetHeight(rightEyeCircle.frame)/SLIDER_MULTIPLIER;
        [_containerView bringSubviewToFront:_rightCircleContainerView];
        [_rightCircleContainerView bringSubviewToFront:rightCircleImgView];
    } else if (_shownControlType == FINAL_VIEW) {
        [self showFinalView];
        slider.hidden = NO;
        slider.value = 1;
    }
}

- (void)hideAllControls {
    
    slider.hidden = YES;
    leftEyeVw.hidden = YES;
    rightEyeVw.hidden = YES;
    leftCircleImgView.hidden = YES;
    rightCircleImgView.hidden = YES;
    leftEyeCircle.hidden = YES;
    rightEyeCircle.hidden = YES;
}

- (void)showFinalView {
    
    self.navigationItem.rightBarButtonItems = nil;
    [self hideAllControls];
    _leftCircleContainerView.layer.mask = leftEyeVw.shapeView;
    leftCircleImgView.hidden = NO;
    [_containerView bringSubviewToFront:_leftCircleContainerView];
    
    _rightCircleContainerView.layer.mask = rightEyeVw.shapeView;
    rightCircleImgView.hidden = NO;
    [_containerView bringSubviewToFront:_rightCircleContainerView];
    
    [self.scrollView zoomToRect:self.view.frame animated:YES];
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    
    return self.contentView;
}

- (void)resetView {
    
    _shownControlType = INITIAL_VIEW;
    for (__strong UIView *eachSubView in _imgView.subviews) {
        [eachSubView removeFromSuperview];
        eachSubView = nil;
    }
    for (__strong UIView *eachSubView in _leftCircleContainerView.subviews) {
        [eachSubView removeFromSuperview];
        eachSubView = nil;
    }
    for (__strong UIView *eachSubView in _rightCircleContainerView.subviews) {
        [eachSubView removeFromSuperview];
        eachSubView = nil;
    }
    [slider removeFromSuperview];
    _leftCircleContainerView.layer.mask = nil;
    _rightCircleContainerView.layer.mask = nil;
}

- (void)fetchEyePositionWithImage:(CIImage *)image {
    
    CGSize expectedSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    CGFloat widthScaleFactor = 0.0;
    CGFloat heightScaleFactor = 0.0;
    if (_imgView.contentMode == UIViewContentModeScaleAspectFit) {
        
        if (image.extent.size.width/_imgView.frame.size.width > image.extent.size.height/_imgView.frame.size.height) {
            expectedSize = CGSizeMake(_imgView.frame.size.width, _imgView.frame.size.width*(image.extent.size.height/image.extent.size.width));
        } else {
            expectedSize = CGSizeMake(_imgView.frame.size.height*(image.extent.size.width/image.extent.size.height), _imgView.frame.size.height);
        }
    }
    widthScaleFactor = image.extent.size.width/expectedSize.width;
    heightScaleFactor = image.extent.size.height/expectedSize.height;

    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:CIDetectorAccuracyHigh, CIDetectorAccuracy, nil];
    
    CIDetector *faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:options];
    
    NSArray *features = [faceDetector featuresInImage:image];
    CGPoint leftEyePosition = CGPointZero;
    CGPoint rightEyePosition = CGPointZero;
    [_imgView setUserInteractionEnabled:YES];
    for (CIFaceFeature *faceFeature in features)
    {
//        CGRect faceSize = faceFeature.bounds;
        
        if (faceFeature.hasLeftEyePosition && CGPointEqualToPoint(leftEyePosition, CGPointZero)) {
            leftEyePosition = faceFeature.leftEyePosition;
            if (_imgView.contentMode == UIViewContentModeScaleAspectFit)
                _leftEyePos = CGPointMake(leftEyePosition.x/widthScaleFactor, (_imgView.frame.size.height-expectedSize.height)/2 + (expectedSize.height-leftEyePosition.y/heightScaleFactor));
            else
                _leftEyePos = CGPointMake(leftEyePosition.x/widthScaleFactor, _imgView.frame.size.height-leftEyePosition.y/heightScaleFactor);

            leftEyeVw = [[ECEyeView alloc] initWithFrame:_containerView.frame withPoint:_leftEyePos];
            [_imgView addSubview:leftEyeVw];
            UILongPressGestureRecognizer *panGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(leftEyeControlMoved:)];
            panGesture.minimumPressDuration = 0.05;
            [leftEyeVw addGestureRecognizer:panGesture];
        }
        if (faceFeature.hasRightEyePosition && CGPointEqualToPoint(rightEyePosition, CGPointZero)) {
            rightEyePosition = faceFeature.rightEyePosition;
            if (_imgView.contentMode == UIViewContentModeScaleAspectFit)
                _rightEyePos = CGPointMake(rightEyePosition.x/widthScaleFactor, (_imgView.frame.size.height-expectedSize.height)/2 + (expectedSize.height-rightEyePosition.y/heightScaleFactor));
            else
                _rightEyePos = CGPointMake(rightEyePosition.x/widthScaleFactor, _imgView.frame.size.height-rightEyePosition.y/heightScaleFactor);
            
            rightEyeVw = [[ECEyeView alloc] initWithFrame:_containerView.frame withPoint:_rightEyePos];
            [_imgView addSubview:rightEyeVw];
            UILongPressGestureRecognizer *panGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(rightEyeControlMoved:)];
            panGesture.minimumPressDuration = 0.05;
            [rightEyeVw addGestureRecognizer:panGesture];
        }
    }
    [self createEyeCircles];
    [self hideAllControls];
    [self handleControls];
}

- (void)createEyeCircles {

    leftEyeCircle = [[UIView alloc] initWithFrame:CGRectMake(_leftEyePos.x-7, _leftEyePos.y-7, 14, 14)];
    [leftEyeCircle.layer setCornerRadius:leftEyeCircle.frame.size.width/2];
    [leftEyeCircle setBackgroundColor:[UIColor blueColor]];
    [leftEyeCircle setAlpha:0.5];
    UILongPressGestureRecognizer *panGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(leftEyeCirlceMoved:)];
    panGesture.minimumPressDuration = 0.05;
    [leftEyeCircle addGestureRecognizer:panGesture];
    [_leftCircleContainerView addSubview:leftEyeCircle];
    
    rightEyeCircle = [[UIView alloc] initWithFrame:CGRectMake(_rightEyePos.x-7, _rightEyePos.y-7, 14, 14)];
    [rightEyeCircle.layer setCornerRadius:rightEyeCircle.frame.size.width/2];
    [rightEyeCircle setBackgroundColor:[UIColor blueColor]];
    [rightEyeCircle setAlpha:0.5];
    panGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(rightEyeCirlceMoved:)];
    panGesture.minimumPressDuration = 0.05;
    [rightEyeCircle addGestureRecognizer:panGesture];
    [_rightCircleContainerView addSubview:rightEyeCircle];
    
    // This has frames/bounds similar to normal blue views.
    leftCircleImgView = [[UIImageView alloc] initWithFrame:leftEyeCircle.frame];
    [leftCircleImgView setImage:[UIImage imageNamed:@"Blue_Eye.png"]];
    [_leftCircleContainerView addSubview:leftCircleImgView];
    
    rightCircleImgView = [[UIImageView alloc] initWithFrame:rightEyeCircle.frame];
    [rightCircleImgView setImage:[UIImage imageNamed:@"Blue_Eye.png"]];
    [_rightCircleContainerView addSubview:rightCircleImgView];
    
    slider = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    [slider addTarget:self action:@selector(sliderMoving) forControlEvents:UIControlEventAllEvents];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * -0.5);
    [self.view addSubview:slider];
    slider.value = 0.5;
    slider.transform = trans;
    
    CGRect frame = slider.frame;
    frame.origin.x = CGRectGetMaxX(self.view.frame)-50;
    frame.origin.y = 100;
    slider.frame = frame;
}

- (void)updateCircleFrameForView:(UIView *)circularView withImgView:(UIImageView *)imgView {
    
    CGRect frame = circularView.frame;
    CGPoint center = circularView.center;
    frame.size.height = slider.value*SLIDER_MULTIPLIER;
    frame.size.width = slider.value*SLIDER_MULTIPLIER;
    [circularView setFrame:frame];
    circularView.center = center;
    [circularView.layer setCornerRadius:circularView.frame.size.width/2];
    
    imgView.center = center;
    [imgView setFrame:frame];
}

#pragma mark Pan Gestures
- (void)leftEyeCirlceMoved:(UILongPressGestureRecognizer *)gesture {
    
    CGPoint point = [gesture locationInView:_imgView];
    leftEyeCircle.center = point;
}

- (void)rightEyeCirlceMoved:(UILongPressGestureRecognizer *)gesture {
    
    CGPoint point = [gesture locationInView:_imgView];
    rightEyeCircle.center = point;
}

- (void)leftEyeControlMoved:(UILongPressGestureRecognizer *)gesture {
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        if (leftEyeVw.areControlsPanning == NO)
            leftEyeVw.isControlViewPanning = YES;
        else
            return;
        leftEyeControlPrevPoint = [gesture locationInView:_containerView];
        
    } else if (gesture.state == UIGestureRecognizerStateChanged) {
        
        if (![leftEyeVw shouldMoveViewForTouchInPoint:[gesture locationInView:_containerView]]) {
            leftEyeVw.isControlViewPanning = NO;
            return;
        }

        CGPoint newPoint = [gesture locationInView:_containerView];
        [leftEyeVw updateFramesWithPosition:CGPointMake(newPoint.x - leftEyeControlPrevPoint.x, newPoint.y-leftEyeControlPrevPoint.y)];
        leftEyeControlPrevPoint = newPoint;
    }
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        leftEyeVw.isControlViewPanning = NO;
    }
}

- (void)rightEyeControlMoved:(UILongPressGestureRecognizer *)gesture {
    
    
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        if (rightEyeVw.areControlsPanning == NO)
            rightEyeVw.isControlViewPanning = YES;
        else
            return;
        rightEyeControlPrevPoint = [gesture locationInView:_containerView];
        
    } else if (gesture.state == UIGestureRecognizerStateChanged) {
        
        if (![rightEyeVw shouldMoveViewForTouchInPoint:[gesture locationInView:_containerView]]) {
            rightEyeVw.isControlViewPanning = NO;
            return;
        }
        
        CGPoint newPoint = [gesture locationInView:_containerView];
        [rightEyeVw updateFramesWithPosition:CGPointMake(newPoint.x - rightEyeControlPrevPoint.x, newPoint.y-rightEyeControlPrevPoint.y)];
        rightEyeControlPrevPoint = newPoint;

    }
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        rightEyeVw.isControlViewPanning = NO;
    }
}

- (void)sliderMoving {
    
    if (_shownControlType == LEFT_EYE_CIRCLE) {
        [self updateCircleFrameForView:leftEyeCircle withImgView:leftCircleImgView];
    } else if (_shownControlType == RIGHT_EYE_CIRCLE) {
        [self updateCircleFrameForView:rightEyeCircle withImgView:rightCircleImgView];
    } else if (_shownControlType == FINAL_VIEW) {
        leftCircleImgView.alpha = slider.value;
        rightCircleImgView.alpha = slider.value;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
