//
//  ECEyeView.h
//  EyeControl
//
//  Created by Shashank Garg on 21/10/16.
//  Copyright © 2016 inzak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ECEyeView : UIView <UIGestureRecognizerDelegate>

@property (strong, nonatomic) CAShapeLayer *shapeView;
@property (assign, nonatomic) BOOL isControlViewPanning;
@property (assign, nonatomic) BOOL areControlsPanning;

-(instancetype)initWithFrame:(CGRect)frame withPoint:(CGPoint)point;
-(void)updateFramesWithPosition:(CGPoint)point;
-(BOOL)shouldMoveViewForTouchInPoint:(CGPoint)point;

@end
